//
//  User+CoreDataProperties.swift
//  PickItUp
//
//  Created by Santiago Dameno on 13/12/18.
//  Copyright © 2018 Santiago Dameno. All rights reserved.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var name: String?

}

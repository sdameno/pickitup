//
//  UserDefaults+Helpers.swift
//  PickItUp
//
//  Created by Santiago Dameno on 1/12/18.
//  Copyright © 2018 Santiago Dameno. All rights reserved.
//

import Foundation

extension UserDefaults{
    
    enum UserDefaultsKeys: String{
        
        case isLoggedIn
    }
}

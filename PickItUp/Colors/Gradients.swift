//
//  Gradients.swift
//  PickItUp
//
//  Created by Santiago Dameno on 13/12/18.
//  Copyright © 2018 Santiago Dameno. All rights reserved.
//

import Foundation
import UIKit

var gradientLayer: CAGradientLayer!
func createGradientLayer(on: UIViewController) {
    gradientLayer = CAGradientLayer()
    gradientLayer.frame = on.view.bounds
    gradientLayer.colors = [UIColor.cyan.cgColor, UIColor.lightGray.cgColor]
    
    on.view.layer.addSublayer(gradientLayer)
}
